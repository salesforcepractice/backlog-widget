({
  // initialisation function - retrieves the board data from the
  // Apex controller
	doInit : function(cmp, ev) {
    var sobjectType = cmp.get('v.SObjectType');
    var stageValueField = cmp.get('v.StageValueField');
    var stageConfigField = cmp.get('v.StageConfigField');
    var fieldNames = cmp.get('v.FieldNames');
    var contactLookupField = cmp.get('v.ContactLookupField');
    var warningIconField = cmp.get('v.WarningIcon');
    var secondIconField = cmp.get('v.SecondIcon');
    var includeValues = cmp.get('v.IncludeValues');
    var excludeValues = cmp.get('v.ExcludeValues');
    var filter = cmp.get('v.Filter');
    var orderBy = cmp.get('v.OrderBy');

		var action = cmp.get("c.GetStages");
    var params = {
      "sobjectType":sobjectType,
      "stageValueField": stageValueField,
      "stageConfigField": stageConfigField,
      "contactLookupField": contactLookupField,
      "warningIconField": warningIconField,
      "secondIconField": secondIconField,
      "includeValues": includeValues,
      "excludeValues": excludeValues,
      "fieldNames": fieldNames,
      "filter": filter,
      "orderBy": orderBy
    };

		window.console.log('Params = ' + JSON.stringify(params));

    action.setParams(params);

    var self = this;
    action.setCallback(this, function(response) {
      try {
        self.actionResponseHandler(response, cmp, self, self.gotStages);
      }
      catch (e) {
        // TODO: We need to get rid of this window.console and use a better notification message
        window.console.log('Exception ' + e);
      }
    });
    $A.enqueueAction(action);
  },

  // generic action method response handler - carries out error checking
  // and assuming successful invokes the supplied callback, including the
  // callback data
	actionResponseHandler : function (response, component, helper, cb, cbData) {
    try {
      var state = response.getState();
			if (state === "SUCCESS") {
        var retVal = response.getReturnValue();
        if (cb) {
          cb(component, helper, retVal, cbData);
        }
			}
			else if (state === "ERROR") {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						window.console.log("Error message: " + errors[0].message);
					}
				}
				else {
					window.console.log("Unknown error");
				}
			}
    }
    catch (e) {
      window.console.log('Exception in actionResponseHandler: ' + e);
    }
  },

  assignWorkItem : function(component, workItemId) {
    var action = component.get("c.CreateAgentWorkRecord");
    action.setParams({
      "workItemId" : workItemId
    });
    var self = this;
    action.setCallback(this, function(response) {
      self.actionResponseHandler(response);
    });
    $A.enqueueAction(action);
  },

  // callback invoked when the board data is retrieved
  gotStages : function(cmp, helper, stages) {
    window.console.log('Got stages ' + JSON.stringify(stages));

    cmp.set('v.Stages', stages);
    cmp.set('v.ColumnWidth', stages.length);

    var useOmni = cmp.get("v.UseOmni");
    if (useOmni) {
      helper.onStatusChange(cmp);
    }
    else {
      helper.enableWidget(cmp);
    }
  },

  // helper method to extract a parameter from the URL
  getURLParameter : function(param, defaultValue) {
    var result = decodeURIComponent((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[""])[1].replace(/\+/g, '%20'))||null;
    if (null == result && defaultValue) {
      result = defaultValue;
    }
    return result;
  },

  checkLightningExperience: function() {
    var isEnabled = true;
    if (window.location.href.indexOf("visual.force.com") > -1) {
      isEnabled = false;
    }
    return isEnabled;
  },

  onStatusChange : function(component) {
    var omniAPI = component.find("omniToolKit");
    var self = this;
    omniAPI.getServicePresenceStatusId().then(function(result) {
      window.console.log('Status Id is: ' + result.statusApiName);

      var statusApiName = result.statusApiName;
      if (statusApiName === 'ASP_BacklogWork') {
        self.enableWidget(component);
      }
      else {
        self.disableWidget(component);
      }
    }).catch(function(error) {
      window.console.log(error);
    });
  },

  createPopover : function(component, contact, selector) {
    $A.createComponent(
      "c:BBSObjectPopover",
      { "ContactInfo": contact },
      function(content, status) {
        if (status === 'SUCCESS') {
          let overlayPromise = component.find('overlayLib').showCustomPopover({
            body: content,
            referenceSelector: selector,
            cssClass: "popover"
          });
          component.set("v.overlayPromise", overlayPromise);
        }
      }
    );
  },

  openTab : function(component, workItemId) {
    if (this.checkLightningExperience()) {
      var workspaceAPI = component.find("workspace");
      var utilityBar = component.find("utilitybar");

      workspaceAPI.openTab({
        url: '#/sObject/' + workItemId + '/view',
        focus: true
      }).catch(function(error) {
        window.console.log(error);
      });
      utilityBar.minimizeUtility();
    } 
    else {
      var myEvent = $A.get("e.c:BBSObjectBoardRecordSelectedEvent");
      myEvent.setParam("recordId", workItemId);
      myEvent.fire();
    }
  },

  enableWidget : function(component) {
    component.set("v.isActive", true);
  },

  disableWidget : function(component) {
    component.set("v.isActive", false);
  }
})