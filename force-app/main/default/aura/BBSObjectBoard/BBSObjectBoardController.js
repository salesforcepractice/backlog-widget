({
  doInit : function(component, event, helper) {
    component.set("v.OrderBy", "LastModifiedDate ASC");
    component.set("v.Filter", "OwnerId = '" + $A.get("$SObjectType.CurrentUser.Id") + "'");

    helper.doInit(component, event);
    
    var win = window.setInterval(
      $A.getCallback(function() {
        helper.doInit(component, event);
      }), 300000
    );
    component.set("v.win", win);
	},

  handleRecordStageChanged: function (component, event, helper) {
    helper.doInit(component, event);
  },

  openTab : function(component, event, helper) {
    if (helper.checkLightningExperience()) {
      var workspaceAPI = component.find("workspace");
      var utilityBar = component.find("utilitybar");
      var id = event.target.id;

      workspaceAPI.openTab({
        url: '#/sObject/'+id+'/view',
        focus: true
      }).catch(function(error) {
        window.console.log(error);
      });
      utilityBar.minimizeUtility();
    } 
    else {
      var myEvent = $A.get("e.c:BBSObjectBoardRecordSelectedEvent");
      myEvent.setParam("recordId", event.target.id);
      myEvent.fire();
    }
  },

  onRecordSelected : function(component, event, helper) {
    var id = event.target.id;
    var useOmni = component.get("v.UseOmni");

    if (useOmni) {
      helper.assignWorkItem(component, id);
    }
    else {
      helper.openTab(component, id);
    }

    if (helper.checkLightningExperience()) {
      var utilityBar = component.find("utilitybar");
      utilityBar.minimizeUtility();
    } 
  },

  refreshData: function(component, event, helper) {
    helper.doInit(component, event);
  },

  handleConsoleStatusChanged: function(component, event, helper) {
    var isEnabled = event.getParam("status");
    var links = component.find("clickableTitle");
    window.console.log('entered handleConsoleStatusChanged');
    if (isEnabled) {
      for(var cmp in links) {
        $A.util.removeClass(links[cmp], "avoid-clicks");
      }
    }
    else {
      for(var comp in links) {
         $A.util.addClass(links[comp], "avoid-clicks");
      }
    }
  },

  onStatusChange: function(component, event, helper) {
    var useOmni = component.get("v.UseOmni");
    if (!useOmni) {
      return;
    }

    helper.onStatusChange(component, helper);
  },

  onLogout: function(component, event, helper) {
    var useOmni = component.get("v.UseOmni");
    if (!useOmni) {
      return;
    }

    helper.disableWidget(component);
  },

  handleShowPopover: function(component, event, helper) {
    let contactJSON = event.currentTarget.dataset.contact;
    if (!contactJSON) {
      return;
    }

    let contact = JSON.parse(contactJSON);
    let selector = "a[id='" +  event.target.id + "'] + span.popover-anchor";

    let overlayPromise = component.get('v.overlayPromise');
    if (overlayPromise) {
      overlayPromise.then(
        overlay => {
          overlay.close();
          component.set("v.overlayPromise", null);

          let overlayTimer = component.get("v.closeOverlayTimer");
          if (overlayTimer) {
            clearTimeout(overlayTimer);
            component.set("v.closeOverlayTimer", null);
          }

          helper.createPopover(component, contact, selector);
        }
      );
    }
    else {
      helper.createPopover(component, contact, selector);
    }
  },

  handleClosePopover: function(component, event, helper) {
    if (!component.get("v.overlayPromise")) {
      return;
    }

    let timeout = setTimeout(() => {
      let overlayPromise = component.get("v.overlayPromise");
      if (overlayPromise) {
        overlayPromise.then(overlay => {
          overlay.close();
          component.set('v.overlayPromise', null);
          component.set("v.closeOverlayTimer", null);
        });
      }
    }, 500);
    component.set("v.closeOverlayTimer", timeout);
  },

  stopClosePopover: function(component, event, helper) {
    let timeout = component.get("v.closeOverlayTimer");
    clearTimeout(timeout);
    component.set("v.closeOverlayTimer", null);
  }
})