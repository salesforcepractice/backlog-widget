({
    doInit : function(component, event, helper) {
        let sObjectTypes = component.get("v.SObjectTypes").split(";");
        let stageValueFields = component.get("v.StageValueFields").split(";");
        let fieldNames = component.get("v.FieldNames").split(";");
        let contactLookupFields = component.get("v.ContactLookupFields").split(";");
        let includeValues = component.get("v.IncludeValues").split(";");
        let excludeValues = component.get("v.ExcludeValues").split(";");

        let boards = [];

        let action = component.get("c.getPluralNames");
        action.setParams({ names : sObjectTypes });
        action.setCallback(this, function(response) {
            let pluralNames = sObjectTypes;
            if (response.getState() === "SUCCESS") {
                pluralNames = response.getReturnValue();
            }

            for (let sObjectType of sObjectTypes) {
                boards.push({
                    sObjectType : sObjectType,
                    pluralName : pluralNames.shift(),
                    stageValueField : stageValueFields.length ? stageValueFields.shift() : "",
                    fieldNames : fieldNames.length ? fieldNames.shift() : "",
                    contactLookupField : contactLookupFields.length ? contactLookupFields.shift() : "",
                    includeValues : includeValues.length ? includeValues.shift() : "",
                    excludeValues : excludeValues.length ? excludeValues.shift() : ""
                });
            }

            component.set("v.boards", boards);
        });

        $A.enqueueAction(action);
    }
})
