(function(ctx) {
  // UI Constants
  var _CLASSNAME_DISABLED = "hidden";

  // Code Constants
  var _COMMAND_LINK_STATE = {
    enabled: "enabled",
    disabled: "disabled"
  };

  // Controller
  ctx.Rjs.define(['lib/jquery', 'lib/moment', 'utils/js/logUtil', 'utils/js/sldsUtil', 'utils/js/roUtil'], function($, moment, LogUtil, SldsUtil, RoUtil) {
    var _currentCommandLinkState = _COMMAND_LINK_STATE.disabled;

    var _caseSO = new SObjectModel.Case();
    var _caseIdToOpen = null;

    LogUtil.info("User ID: {0}", ctx.ForceUI.UserId);

    function createOpenCaseIcon(caseId) {
      return $(
        '<span class="slds-icon_container open-case-icon" style="padding-right: 5px">' +
          '<svg aria-hidden="true" class="slds-icon slds-icon_small slds-icon-standard-groups">' +
            '<use xlink:href="' + ctx.ForceUI.SLDS.BaseUrl + '/assets/icons/utility-sprite/svg/symbols.svg#open"></use>' +
          '</svg>' +
          '<span class="slds-assistive-text">Open Case</span>' +
        '</span>')
        .click(function () {
          _caseIdToOpen = caseId;

          ASP_AgentCaseBacklogController.createAgentWorkRecord(
            caseId,
            function (result, event) {
              LogUtil.info("CreateAgentWorkRecord request status: {0}", event.status);
            },
            { escape: true }
          );
        });
    }

    function acceptWorkIfBacklogCase(workId, workItemId) {
      if (!_caseIdToOpen) {
        LogUtil.info('Workitem was not opened through the backlog');
        return;
      }

      if (_caseIdToOpen.substring(0, 15) === workItemId.substring(0, 15)) {
        //Now that we have the assigned work item ID, we can accept it
        sforce.console.presence.acceptAgentWork(workId, function(result) {
          _caseIdToOpen = null;
          if (result.success) {
            LogUtil.debug('Accepted work successfully');
          } else {
            LogUtil.debug('Failed to accept the workitem, it might be auto-accepted');
          }
        });
      }  else {
        LogUtil.info('Workitem is not the case that was opened through the backlog');
        _caseIdToOpen = null;
      }
    }

    function createTableRowForCase(rec) {
      return $('<tr></tr>')
        .append($('<th scope="row"></th>').append(SldsUtil.createTruncatedDivWithText(rec.get("Subject"))))
        .append($('<th scope="row"></th>').text(rec.get("Priority")))
        .append($('<th scope="row"></th>').text(rec.get("Origin")))
        .append($('<td><div class="slds-truncate"></div></td>').text(moment(rec.get("CreatedDate")).format("ll")))
        .append($('<td></td>').append(createOpenCaseIcon(rec.get("Id"))));
    }

    function setCommandLinkState(state) {
      LogUtil.debug("Setting commandLink state to: {0}", state);
      _currentCommandLinkState = state;

      var $commandLinks = $("span.open-case-icon");
      LogUtil.debug("# of command links: {0}", $commandLinks.length);

      var isEnabled = _COMMAND_LINK_STATE.enabled === (state || "").toLowerCase();
      if (isEnabled) {
        $commandLinks.css('display', '');
      } else {
        $commandLinks.css('display', 'none');
      }

      if ($commandLinks.length > 0) {
        if (isEnabled) {
          $("#omni-invalid-state-msg").addClass("hidden");
        } else {
          $("#omni-invalid-state-msg").removeClass("hidden");
        }
      }
    }

    function setCurrentCommandLinkState() {
      setCommandLinkState(_currentCommandLinkState);
    }

     function getCaseBacklogState(info) {
      var caseBacklogState = _COMMAND_LINK_STATE.disabled;
      var channels = JSON.parse(info.channels);

      var i, len = channels.length;
      for (i = 0; i < len; i++) {
        var theChannel = channels[i];
        LogUtil.debug("Channel name: {0}", theChannel.developerName);
        if (theChannel.developerName === "ASP_AgentBacklogChannel") {
          caseBacklogState = _COMMAND_LINK_STATE.enabled;
        }
      }
      return caseBacklogState;
    }

    function refreshPageList() {
      LogUtil.debug("Refreshing page list");
      RoUtil.query(_caseSO, {
          where: {
           OwnerId: {eq: ctx.ForceUI.UserId},
           IsClosed: {eq: false}
          },
          orderby: [ {Priority: 'DESC'}, {CreatedDate: 'ASC'} ],
          limit: 10
        }).
        then(function (records) {
          $("#backlogDetails").empty();

          var i, len = records.length;

          LogUtil.debug("Number of records to display: {0}", len);
          if (len === 0) {
            $("#no-records-msg").removeClass("hidden");
            $("#backlogTable").addClass("hidden");
          } else {
            $("#no-records-msg").addClass("hidden");
            $("#backlogTable").removeClass("hidden");
          }

          for (i = 0; i < len; i++) {
            var rec = records[i];

            var $row = createTableRowForCase(rec);

            $("#backlogDetails").append($row);
          }
        }).
        then(function () {
          $("#backlog").removeClass("hidden");
        }).
        then(setCurrentCommandLinkState).
        catch(function (ex) {
          LogUtil.error("ERROR refreshing the page list: {0}", JSON.stringify(ex));
        });
    }

    LogUtil.debug('Getting current omni status');
    sforce.console.presence.getServicePresenceStatusChannels(function(result) {
      if (result.success) {
        LogUtil.debug("Current omni status received: {0}", JSON.stringify(result));
        setCommandLinkState(getCaseBacklogState(result));
      } else {
        LogUtil.warn('Retrieving current channel status failed - might be offline');
        setCommandLinkState(_COMMAND_LINK_STATE.disabled);
      }
    });

    LogUtil.debug('Initializing Omni state listener');
    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.STATUS_CHANGED, function (ev) {
      LogUtil.debug("Omni state change received: {0}", JSON.stringify(ev));
      setCommandLinkState(getCaseBacklogState(ev));
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_ACCEPTED, function (ev) {
      LogUtil.debug("New workitem accepted: {0}", JSON.stringify(ev));
      refreshPageList();
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_ASSIGNED, function (ev) {
      LogUtil.debug("New workitem assigned: {0}", JSON.stringify(ev));
      acceptWorkIfBacklogCase(ev.workId, ev.workItemId);
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.LOGOUT, function (ev) {
      LogUtil.debug("Omni logout received: {0}", JSON.stringify(ev));
      setCommandLinkState(_COMMAND_LINK_STATE.disabled);
    });

    sforce.console.addEventListener(sforce.console.ConsoleEvent.CLOSE_TAB, function (ev) {
      LogUtil.debug("Tab close received: {0}", JSON.stringify(ev));
      if (ev.objectId && ev.objectId.startsWith('500')) {
        refreshPageList();
      }
    });

    LogUtil.debug('Initializing Case push listener');
    sforce.console.addPushNotificationListener(['Case'], function (ev) {
      LogUtil.debug("Received push event: {0}", JSON.stringify(ev));
      refreshPageList();
    });

    refreshPageList();
  });
})(this);