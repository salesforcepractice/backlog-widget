(function(ctx) {
  console.log('ASP_PostWorkUpdateInteraction controller loaded');
  ctx.Rjs.require(['lib/jquery', 'utils/js/logUtil', 'lib/moment'], function($, LogUtil) {
    LogUtil.setPageId('ASP_PostWorkUpdateInteraction.page');

    var primaryTabId;
    sforce.console.getEnclosingPrimaryTabId(function(result) {
      primaryTabId = result.id;
    });

    sforce.console.onFocusedSubtab(function(result) {
      var tabId = result.id;
      var objId = result.objectId;

      if (objId && (objId.startsWith('500') || objId.startsWith('006') || objId.startsWith('00Q'))) {
        setTimeout(function() {
          sforce.console.getSubtabIds(primaryTabId, function(subtabIdResult) {
            if (subtabIdResult.success && subtabIdResult.ids.includes(tabId)) {
              sforce.console.getPageInfo(tabId, function(pageInfoResult) {
                var pageInfo = JSON.parse(pageInfoResult.pageInfo);
                var tabName = pageInfo.objectName;

                var selectWrapperId, hiddenSpanClass;
                switch(objId.substring(0, 3)) {
                  case "500":
                    selectWrapperId = 'caseSelect';
                    hiddenSpanClass = 'caseHiddenRight';
                    break;
                  case "006":
                    selectWrapperId = 'oppSelect';
                    hiddenSpanClass = 'oppHiddenRight';
                    break;
                  case "00Q":
                    selectWrapperId = 'leadSelect';
                    hiddenSpanClass = 'leadHiddenRight';
                    break;
                }

                var leftBox = $('#' + selectWrapperId + ' select:first');
                var rightBox = $('#' + selectWrapperId + ' select:last');

                if (!leftBox.html().includes(objId) && !rightBox.html().includes(objId)){
                  rightBox.append('<option value="' + objId + '">' + tabName + '</option>');
                  // call internal JS method in MultiselectPicklist.component to build hidden string
                  buildOutputString(rightBox.get(0), $('.' + hiddenSpanClass).get(0));
                }
              });
            }
          });
        }, 500);
      }
    });

    $('#saveBtn').click(saveRecord);

    function saveRecord(){

      LogUtil.info('ASP_PostWorkUpdateInteraction.saveRecord invoked');

      var account = $('[id*="AccountSelector"]').val();
      var contact = $('[id*="ContactSelector"]').val();
      var interactionSegmentId = $('#interaction_segment_Id').val();
      var cases = $('[class*="caseHiddenRight"]').val();
      var opps = $('[class*="oppHiddenRight"]').val();
      var leads = $('[class*="leadHiddenRight"]').val();
      var disposition = $('[id*="callDisposition"]').val();
      var notes = $('[id*="txtNotes"]').val();

      var properties = JSON.stringify({
        account: account,
        contact: contact,
        cases: cases,
        opps: opps,
        leads: leads,
        interactionSegmentId: interactionSegmentId,
        disposition: disposition,
        notes: notes
      });

      LogUtil.info('ASP_PostWorkUpdateInteraction.VF remoting invoked');
      Visualforce.remoting.Manager.invokeAction(
        'ASP_PostWorkUpdateInteractionController.quickSave',
        properties,
        function(result, event) {
          LogUtil.info('ASP_PostWorkUpdateInteraction.VF remoting close tab function fired');
          if (sforce.console && sforce.console.isInConsole()) {
            sforce.console.getFocusedSubtabId(function(result) {
              sforce.console.disableTabClose(false, result.id);
              sforce.console.closeTab(result.id);
            });
            sforce.console.getFocusedPrimaryTabId(function(result) {             
              sforce.console.closeTab(result.id);
            });
          } else {
            document.getElementById('onSaveCompleted').classList.remove('hidden');
            setTimeout(function () {
              document.getElementById('onSaveCompleted').classList.add('hidden');
            }, 3000);
          }
        }
      );
    }

  });
})(this);