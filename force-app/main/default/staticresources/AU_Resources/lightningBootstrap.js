// to be loaded with bootstrap.js for Lightning components
(function(ctx) {
  ctx.Rjs.require.config({
    shim: {
      'lib/cometd-core': {
        deps:[],
        exports: 'org'
      },
      'lib/jquery': {
        deps:[],
        exports: 'jQuery'
      },
      'lib/cometd': {
        deps:['lib/jquery', 'lib/cometd-core'],
        exports: 'jQuery'
      },
      'lib/moment': {
        deps:[],
        exports: 'moment'
      },
      'lib/promise': {
        deps:[],
        exports: 'Promise'
      },
      'lib/underscore': {
        deps:[],
        exports: '_'
      }
    }
  });

  window.aria = {};
  window.aria.Rjs = ctx.Rjs;

})(this);
