/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belowing to 
 * the customer require a written permission from Aria Solutions. 
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery'], function ($) {
    function createTruncatedDivWithText(text) {
      return $('<div class="slds-truncate"></div>').html(text);
    }

    return {
      createTruncatedDivWithText: createTruncatedDivWithText
    }
  });
})(this);