window.aria_ltngUtil = (function(){
    return {
      showToast : function(title, type, message) {
         var toastEvent = $A.get("e.force:showToast");
         toastEvent.setParams({
             "title": title,
             "type": type,
             "message": message
         });
         toastEvent.fire();
       },
        
      createActionCallback: function(logger, successCallback, errorCallback, incompleteCallback) {
        
        return function(response) {
         var state = response.getState();
         switch(state)	{
           case "SUCCESS":
             if (successCallback) {
               successCallback(response);
               break;
             }
             logger.info("Request successful");
             break;
           case "ERROR":
             if (errorCallback) {
              errorCallback(response);
              break;
             }
             var errors = response.getError();
             var errorMessage = '';
             if (errors) {
               for(var i = 0; i < error.length; i++)  {
                 if (errors[i] && errors[i].message) {
                  errorMessage += errors[i].message + ";";
                 }
               }
               logger.error("Request failed:" + errorMessage);
             } else {
               logger.error("Request failed:Unknown error");
             }
             break;
           case "INCOMPLETE":
             if (incompleteCallback) {
              incompleteCallback(response);
              break;
             }
             logger.warn("Request incomplete");
             break;
         }
        }
      },
	  
      createSaveResult: function(logger, successCallback, errorCallback, incompleteCallback)	{

        return function(saveResult)	{
          switch(saveResult.state)	{
            case "SUCCESS":
             if (successCallback) {
               successCallback(saveResult);
               break;
             }
             logger.info("Request successful");
             break;
            case "ERROR":
             if (errorCallback) {
              errorCallback(saveResult);
              break;
             }
             logger.error("Problem saving contact, error: " + JSON.stringify(saveResult.error));
             break;
            case "INCOMPLETE":
             if (incompleteCallback) {
              incompleteCallback(saveResult);
              break;
             }
             logger.warn("Request incomplete");
             break;
            default:
             logger.error("Unknown problem, state: " + saveResult.state + ", error:" + JSON.stringify(saveResult.error));
             break;
          }
        }
      },
	  
      navigateToUrl: function(url)	{
	var urlEvent = $A.get("e.force:navigateToURL");
	urlEvent.setParams({
	  "url" : url,
	  "isredirect" : false
	});
	urlEvent.fire();  
      }
    };
}());