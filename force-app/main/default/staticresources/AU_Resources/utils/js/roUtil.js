/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/promise'], function (Promise) {
    function query(sObject, args) {
      return new Promise(function (resolve, reject) {
        sObject.retrieve(
          args,
          function(err, records) {
            if (err) {
              reject(err);
            } else {
              resolve(records);
            }
          }
        );
      });
    }

    return {
      query: query
    };
  });
})(this);