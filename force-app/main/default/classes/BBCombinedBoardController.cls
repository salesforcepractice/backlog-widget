public class BBCombinedBoardController {
    @AuraEnabled
    public static List<String> getPluralNames(List<String> names) {
        List<String> pluralNames = new List<String>();
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        
        for (String name : names) {
            SObjectType type = gd.get(name);
            if (type != null) {
                String pluralName = type.getDescribe().getLabelPlural();
                pluralNames.add(pluralName);
            }
            else {
                pluralNames.add(name);
            }
        }

        return pluralNames;
    }
}
