/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */


public class BBSObjectPopoverApexController {

	@AuraEnabled
	public static Contact getContact(Id caseId){
		
		Case c = [SELECT ContactId FROM Case WHERE Id =: caseId];
		
		return [SELECT Id, Phone, Email, MailingStreet, MailingState, MailingCity, MailingPostalCode, Name, Title FROM Contact WHERE Id =: c.ContactId];
	}

}